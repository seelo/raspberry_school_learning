import wiringpi as gpio
from wiringpi import GPIO
SENSOR_PIN = 23

# One of the following MUST be called before using IO functions:
gpio.wiringPiSetupGpio()  # For GPIO pin numbering  使用BCM编号

gpio.pinMode(SENSOR_PIN, GPIO.INPUT)
#gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_OFF)
gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_UP)
#gpio.pullUpDnControl(SENSOR_PIN, GPIO.PUD_DOWN)
while 1:
    state = gpio.digitalRead(SENSOR_PIN)
    print(state)
    if state == GPIO.LOW :
        print("SENSOR LOW ALARM")
   
    gpio.delay(500)
