# 05.树莓派wiringPi python的digitalWrite的学习和测试

今天仔细看下wiringPi Python的代码，体会了下，原来python版本的wiringPi就是去调用c版本的，所以说执行速度肯定是要慢，一般对于时间精度不高的应用，感觉是够了。另外用python的主要好处是处理和以后做其他应用方便。所以想仔细体会下这个python版本的wiringPi使用，网上查了下资料很少，基本没有。只能自己摸索了。

生命在于折腾，什么都有的学着就没意思了，我选择了用python，也顺便锻炼下python，前一段自学了点python，只是入门级。而c基本用了好多年了，直接用wiringPi没什么困难。

正式开始，上一讲的代码led.py如下

```
import wiringpi

# One of the following MUST be called before using IO functions:
wiringpi.wiringPiSetup()      # For sequential pin numbering
# OR
# wiringpi.wiringPiSetupSys()   # For /sys/class/gpio with GPIO pin numbering
# OR
# wiringpi.wiringPiSetupGpio()  # For GPIO pin numbering

wiringpi.pinMode(25, 1)       # Set pin 26 to 1 ( OUTPUT )
while 1:
    wiringpi.digitalWrite(25, 1)  # Write 1 ( HIGH ) to pin 26
    wiringpi.delay(500)
    wiringpi.digitalWrite(25, 0)  # Write 0 ( LOW ) to pin 26
    wiringpi.delay(500)
	
```

修正下让这个代码更容易读懂,使用GPIO这个类，调用里面对输入输出的定义和高低电平的定义

```
import wiringpi as gpio
from wiringpi import GPIO
LED_PIN = 25

# One of the following MUST be called before using IO functions:
gpio.wiringPiSetup()      # For sequential pin numbering
# OR
# wiringpi.wiringPiSetupSys()   # For /sys/class/gpio with GPIO pin numbering
# OR
# wiringpi.wiringPiSetupGpio()  # For GPIO pin numbering

gpio.pinMode(LED_PIN, GPIO.OUTPUT)       # Set pin 26 to 1 ( OUTPUT )
while 1:
    gpio.digitalWrite(LED_PIN, GPIO.HIGH)  # Write 1 ( HIGH ) to pin 26
    gpio.delay(500)
    gpio.digitalWrite(LED_PIN, GPIO.LOW)  # Write 0 ( LOW ) to pin 26
    gpio.delay(500)
```

测试digitalWrite功能，用于输出GPIO的状态，是HIGH还是LOW，常用于控制LED开关，蜂鸣器的开关，继电器的开关等等常用的数字量开关的元件

常用器件可以在下面链接搜索购买：https://ilovemcu.taobao.com

